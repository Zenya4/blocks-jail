package com.zenya.blocksjail.storage;

import com.zenya.blocksjail.utilities.DBManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;

public class DataCache {
    private static DataCache dataCache;
    private static DBManager dbManager = DBManager.getInstance();
    private HashMap<Player, Integer> data = new HashMap<Player, Integer>();

    public DataCache() {
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            registerPlayer(player, dbManager.getBlocksLeft(player));
        }
    }

    public Set<Player> getKeys() {
        return data.keySet();
    }

    public Integer getBlocksLeft(Player player) {
        return data.get(player);
    }

    public void registerPlayer(Player player, Integer blocksLeft) {
        data.put(player, blocksLeft);
    }

    public void unregisterPlayer(Player player) {
        data.remove(player);
    }

    public static DataCache getInstance() {
        if(dataCache == null) {
            dataCache = new DataCache();
        }
        return dataCache;
    }
}
