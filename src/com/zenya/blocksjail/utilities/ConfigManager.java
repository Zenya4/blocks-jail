package com.zenya.blocksjail.utilities;

import com.zenya.blocksjail.main.BlocksJail;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldType;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ConfigManager {
    private static ConfigManager configManager;
    private static FileConfiguration config;
    private static Plugin plugin = BlocksJail.getInstance();

    private ConfigManager() {
        this.config = plugin.getConfig();

        if(!(this.getConfigExists())) {
            plugin.saveDefaultConfig();
        }

        if(this.getConfigVersion() != 2) {
            File configFile = new File(plugin.getDataFolder(), "config.yml");
            File oldConfigFile = new File(plugin.getDataFolder(), "config.yml.old");

            configFile.renameTo(oldConfigFile);
            configFile.delete();

            plugin.saveDefaultConfig();
        }
    }

    public void updateConfig() {
        try {
            File configFile = new File(plugin.getDataFolder(), "config.yml");
            File newConfigFile = new File(plugin.getDataFolder(), "config.yml.new");

            if (configFile != null) {
                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(newConfigFile);
                for (String key : defConfig.getConfigurationSection("").getKeys(false))
                    if (!config.contains(key)) config.set(key, defConfig.getConfigurationSection(key));

                for (String key : config.getConfigurationSection("").getKeys(false))
                    if (!defConfig.contains(key)) config.set(key, null);

                plugin.saveConfig();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean getConfigExists() {
        File configFile = new File(plugin.getDataFolder(), "config.yml");
        return configFile.exists();
    }

    public Integer getConfigVersion() {
        Integer version = config.getInt("config-version");
        return version;
    }

    public static class JailWorld {
        private static JailWorld jailWorld;

        public String getName() {
            String name = config.getString("world.name");
            return name;
        }

        public WorldType getType() {
            String typeStr = config.getString("world.type");

            WorldType wt = WorldType.valueOf(typeStr.toUpperCase());
            if(wt == null) {
                wt = WorldType.NORMAL;
            }
            return wt;
        }

        public World.Environment getEnvironment() {
            String envStr = config.getString("world.environment");
            World.Environment env = World.Environment.valueOf(envStr.toUpperCase());
            if(env == null) {
                env = World.Environment.NORMAL;
            }
            return env;
        }

        public static JailWorld getInstance() {
            if(jailWorld == null) {
                jailWorld = new JailWorld();
            }
            return jailWorld;
        }
    }

    public JailWorld getJailWorld() {
        return JailWorld.getInstance();
    }

    public boolean getClearInventory() {
        Boolean clearInv = config.getBoolean("clear-inventory");
        return clearInv;
    }

    public int getUnjailBlocks() {
        Integer unjailBlocks = config.getInt("unjail-blocks");
        return unjailBlocks;
    }

    public List<Material> getBlacklistedMaterials() {
        List<String> blacklistMatStr = config.getStringList("blacklisted-materials");

        if(config.getStringList("blacklisted-materials") == null || config.getStringList("blacklisted-materials").size() == 0) {
            return null;
        }

        List<Material> blacklistMat = new ArrayList<Material>();

        for(String matStr : blacklistMatStr) {
            Material mat = Material.valueOf(matStr.toUpperCase());
            if(mat != null) {
                blacklistMat.add(mat);
            }
        }
        return blacklistMat;
    }

    public boolean getCanDropBlocks() {
        boolean canDropBlocks = config.getBoolean("drop-blocks");
        return canDropBlocks;
    }

    public List<String> getBlacklistedCommands() {
        List<String> blacklistedCommands = config.getStringList("blacklisted-commands");
        return blacklistedCommands;
    }

    public static ConfigManager getInstance() {
        if(configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }
}
