package com.zenya.blocksjail.utilities;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.zenya.blocksjail.main.BlocksJail;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class DBManager {
    private static DBManager dbManager;
    private static Plugin plugin = BlocksJail.getInstance();

    public DBManager() {
        if(!(getDatabaseExists())) {
            plugin.getDataFolder().mkdir();
        }

        this.createTables();
    }

    private boolean getDatabaseExists() {
        File databaseFile = new File(plugin.getDataFolder(), "database.db");
        return databaseFile.exists();
    }

    private Connection connect() {
        String url = "jdbc:sqlite:" + plugin.getDataFolder() + File.separator + "database.db";
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    private void createTables() {
        String sql = "CREATE TABLE IF NOT EXISTS playerdata ("
                + "id integer PRIMARY KEY AUTOINCREMENT, "
                + "player text NOT NULL UNIQUE, "
                + "is_jailed tinyint NOT NULL, "
                + "jailed_by text NOT NULL, "
                + "blocks_left int NOT NULL, "
                + "blocks_total int NOT NULL);";

        try {
            Connection conn = this.connect();
            Statement statement = conn.createStatement();
            statement.execute(sql);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void initPlayerData(Player player) {
        String playername = player.getName();

        String sql = "INSERT OR IGNORE INTO playerdata(player, is_jailed, jailed_by, blocks_left, blocks_total) VALUES(?, ?, ?, ?, ?)";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ps.setInt(2, 0);
            ps.setString(3, "None");
            ps.setInt(4, 0);
            ps.setInt(5, 0);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean getIsJailed(OfflinePlayer player) {
        Boolean isJailed;
        Integer isJailedInt = 0;
        String playername = player.getName();

        String sql = "SELECT is_jailed FROM playerdata WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                isJailedInt = rs.getInt("is_jailed");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        switch(isJailedInt) {
            case 0:
                isJailed = false;
                break;
            case 1:
                isJailed = true;
                break;
            default:
                isJailed = false;
                break;
        }
        return isJailed;
    }

    public String getJailedBy(OfflinePlayer player) {
        String jailedByStr = "";
        String playername = player.getName();

        String sql = "SELECT jailed_by FROM playerdata WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                jailedByStr = rs.getString("jailed_by");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return jailedByStr;
    }

    public int getBlocksLeft(OfflinePlayer player) {
        Integer blocksLeft = 0;
        String playername = player.getName();

        String sql = "SELECT blocks_left FROM playerdata WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                blocksLeft = rs.getInt("blocks_left");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return blocksLeft;
    }

    public int getBlocksTotal(OfflinePlayer player) {
        Integer blocksTotal = 0;
        String playername = player.getName();

        String sql = "SELECT blocks_total FROM playerdata WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, playername);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                blocksTotal = rs.getInt("blocks_total");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return blocksTotal;
    }

    public void setIsJailed(OfflinePlayer player, Boolean setJail) {
        String playername = player.getName();
        Integer setJailInt;

        String sql = "UPDATE playerdata SET is_jailed = ? WHERE player = ?";

        if(setJail) {
            setJailInt = 1;
        } else {
            setJailInt = 0;
        }

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, setJailInt);
            ps.setString(2, playername);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setJailedBy(OfflinePlayer player, CommandSender jailer) {
        String playername = player.getName();
        String jailername;

        if(jailer != null) {
            jailername = jailer.getName();
        } else {
            jailername = "None";
        }

        String sql = "UPDATE playerdata SET jailed_by = ? WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, jailername);
            ps.setString(2, playername);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setBlocksLeft(OfflinePlayer player, Integer blocksLeft) {
        String playername = player.getName();
        if(blocksLeft == null) {
            blocksLeft = 0;
        }

        String sql = "UPDATE playerdata SET blocks_left = ? WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, blocksLeft);
            ps.setString(2, playername);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setBlocksTotal(OfflinePlayer player, Integer blocksTotal) {
        String playername = player.getName();

        String sql = "UPDATE playerdata SET blocks_total = ? WHERE player = ?";

        try {
            Connection conn = this.connect();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, blocksTotal);
            ps.setString(2, playername);
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modBlocksLeft(OfflinePlayer player, Integer modBlocks) {
        Integer oldBlocksLeft = getBlocksLeft(player);
        Integer newBlocksLeft = oldBlocksLeft + modBlocks;

        setBlocksLeft(player, newBlocksLeft);
    }

    public static DBManager getInstance() {
        if(dbManager == null) {
            dbManager = new DBManager();
        }
        return dbManager;
    }
}
