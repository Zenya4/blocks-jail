package com.zenya.blocksjail.utilities;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

public class WorldGenerator {
    private static WorldGenerator worldGenerator;

    public boolean getWorldExists(String name) {
        for(World world : Bukkit.getServer().getWorlds()) {
            if(world.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void createWorld(String name, WorldType wt, World.Environment env) {
        WorldCreator wc = new WorldCreator(name);
        wc.type(wt);
        wc.environment(env);
        wc.createWorld();

    }

    public static WorldGenerator getInstance() {
        if(worldGenerator == null) {
            worldGenerator = new WorldGenerator();
        }
        return worldGenerator;
    }
}
