package com.zenya.blocksjail.utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatUtils {

    public static String translateColor(String message) {
        message = ChatColor.translateAlternateColorCodes('&', message);
        return message;
    }

    public static void sendMessage(Player player, String message) {
        message = translateColor(message);
        player.sendMessage(message);
    }

    public static void sendMessage(CommandSender sender, String message) {
        message = translateColor(message);
        sender.sendMessage(message);
    }

    public static void sendBroadcast(String message) {
        message = translateColor(message);
        Bukkit.getServer().getConsoleSender().sendMessage(message);
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            player.sendMessage(message);
        }
    }

    public static void sendTitle(Player player, String title) {
        title = translateColor(title);
        player.resetTitle();
        player.sendTitle(title, null, 10, 40, 20);
    }

    public static void sendSubtitle(Player player, String subtitle) {
        subtitle = translateColor(subtitle);
        player.resetTitle();
        player.sendTitle(null, subtitle, 0, 10, 0);
    }
}
