package com.zenya.blocksjail.scheduler;

import com.zenya.blocksjail.main.BlocksJail;
import com.zenya.blocksjail.storage.DataCache;
import com.zenya.blocksjail.utilities.ChatUtils;
import com.zenya.blocksjail.utilities.ConfigManager;
import com.zenya.blocksjail.utilities.DBManager;
import com.zenya.blocksjail.utilities.HotbarMessage;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class JailTask {
    private static JailTask jailTask;
    private static ConfigManager configManager = ConfigManager.getInstance();
    private static DBManager dbManager = DBManager.getInstance();
    private static DataCache dataCache = DataCache.getInstance();
    //private static HotbarMessage hotbarMessage = HotbarMessage.getInstance();

    public JailTask() {
        enforceJail();
        enforceUnjail();
        sendJailPacket();
        syncToDB();
    }

    String jailWorld = configManager.getJailWorld().getName();

    public void enforceJail() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Player player : Bukkit.getServer().getOnlinePlayers()) {
                    // Bypass perm
                    if(player.hasPermission("blocksjail.bypass")) {
                        continue;
                    }

                    // Jailed player in another world
                    Integer blocksLeft = dataCache.getBlocksLeft(player);
                    if(!player.getWorld().getName().equals(jailWorld) && blocksLeft != null && blocksLeft > 0) {
                        ChatUtils.sendTitle(player, "&cYou have been jailed");
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                player.teleport(Bukkit.getWorld(jailWorld).getSpawnLocation());
                            }
                        }.runTask(BlocksJail.getInstance());
                    }
                }
            }
        }.runTaskTimerAsynchronously(BlocksJail.getInstance(), 0, 20L);
    }

    public void enforceUnjail() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Player player : Bukkit.getServer().getOnlinePlayers()) {
                    // Bypass perm
                    if(player.hasPermission("blocksjail.bypass")) {
                        continue;
                    }

                    // Unjailed player in jail world
                    Integer blocksLeft = dataCache.getBlocksLeft(player);
                    if(player.getWorld().getName().equals(jailWorld) && (blocksLeft == null || blocksLeft <= 0)) {
                        ChatUtils.sendTitle(player, "&aYou have been unjailed");
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
                            }
                        }.runTask(BlocksJail.getInstance());
                    }
                }
            }
        }.runTaskTimerAsynchronously(BlocksJail.getInstance(), 0, 20L);
    }

    public void sendJailPacket() {
        new BukkitRunnable() {

            @Override
            public void run() {
                for(Player player : Bukkit.getServer().getOnlinePlayers()) {
                    if(player.hasPermission("blocksjail.bypass")) {
                        continue;
                    }

                    // Send packets to jailed players
                    Integer blocksLeft = dataCache.getBlocksLeft(player);
                    if(player.getWorld().getName().equals(jailWorld) && blocksLeft != null && blocksLeft > 0) {
                        String message = "&cJailed >> " + blocksLeft + " blocks remaining";
                        ChatUtils.sendSubtitle(player, message);
                    }
                }
            }
        }.runTaskTimerAsynchronously(BlocksJail.getInstance(), 0L, 5L);
    }

    public void syncToDB() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Player player : dataCache.getKeys()) {
                    Integer blocksLeft = dataCache.getBlocksLeft(player);
                    dbManager.setBlocksLeft(player, blocksLeft);

                    if(blocksLeft <= 0) {
                        dataCache.unregisterPlayer(player);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                dbManager.setIsJailed(player, false);
                                dbManager.setBlocksTotal(player, 0);
                                dbManager.setJailedBy(player, null);
                            }
                        }.runTaskAsynchronously(BlocksJail.getInstance());
                    }
                }
            }
        }.runTaskTimerAsynchronously(BlocksJail.getInstance(), 0L, 20L);
    }

    public static JailTask getInstance() {
        if(jailTask == null) {
            jailTask = new JailTask();
        }
        return jailTask;
    }
}
