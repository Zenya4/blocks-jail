package com.zenya.blocksjail.main;

import com.zenya.blocksjail.commands.JailCommand;
import com.zenya.blocksjail.commands.JailStatusCommand;
import com.zenya.blocksjail.commands.UnjailCommand;
import com.zenya.blocksjail.events.JailBlockReduceEvent;
import com.zenya.blocksjail.events.Listeners;
import com.zenya.blocksjail.scheduler.JailTask;
import com.zenya.blocksjail.utilities.ConfigManager;
import com.zenya.blocksjail.utilities.WorldGenerator;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class BlocksJail extends JavaPlugin {
    private static BlocksJail blocksJail;

    public void onEnable() {
        blocksJail = this;

        // Init Config & DB
        ConfigManager configManager = ConfigManager.getInstance();
        WorldGenerator worldGenerator = WorldGenerator.getInstance();

        // Init Listeners & Commands
        Bukkit.getPluginManager().registerEvents(new Listeners(), this);
        this.getCommand("jail").setExecutor(new JailCommand());
        this.getCommand("unjail").setExecutor(new UnjailCommand());
        this.getCommand("jailstatus").setExecutor(new JailStatusCommand());

        // Generate world if not exists
        if(!worldGenerator.getWorldExists(configManager.getJailWorld().getName())) {
            worldGenerator.createWorld(configManager.getJailWorld().getName(), configManager.getJailWorld().getType(), configManager.getJailWorld().getEnvironment());
        }

        // Run tasks
        new JailTask();
    }

    public void onDisable() {

    }

    public static BlocksJail getInstance() {
        return blocksJail;
    }
}
