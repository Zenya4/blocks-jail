package com.zenya.blocksjail.commands;

import com.zenya.blocksjail.main.BlocksJail;
import com.zenya.blocksjail.storage.DataCache;
import com.zenya.blocksjail.utilities.ChatUtils;
import com.zenya.blocksjail.utilities.DBManager;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class UnjailCommand implements CommandExecutor {
    private static DBManager dbManager = DBManager.getInstance();
    private static DataCache dataCache = DataCache.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.hasPermission("blocksjail.unjail")) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        OfflinePlayer player = null;
        String reason = "";

        if(args.length >= 2) {
            String[] reasonList = args;
            reasonList = (String[]) ArrayUtils.remove(reasonList, 0);

            for(String str : reasonList) {
                reason = reason + " " + str;
            }
        }

        if(args.length >= 1) {
            for(OfflinePlayer ofp : Bukkit.getOfflinePlayers()) {
                if(args[0].equals(ofp.getName())) {
                    player = ofp;
                }
            }

            if(player == null) {
                ChatUtils.sendMessage(sender, "&cPlayer does not exist on this server");
                return true;
            }
        }

        else {
            return false;
        }

        if(!dbManager.getIsJailed(player)) {
            ChatUtils.sendMessage(sender, "&cThat player is not jailed");
            return true;
        }

        if(player.isOnline()) {
            dataCache.unregisterPlayer((Player) player);
        }

        OfflinePlayer finalPlayer = player;
        new BukkitRunnable() {
            @Override
            public void run() {
                dbManager.setIsJailed(finalPlayer, false);
                dbManager.setBlocksLeft(finalPlayer, 0);
                dbManager.setBlocksTotal(finalPlayer, 0);
                dbManager.setJailedBy(finalPlayer, null);
            }
        }.runTaskAsynchronously(BlocksJail.getInstance());

        String unjailMsg = "&a" + sender.getName() + " has unjailed &2" + player.getName();
        if(!reason.equals("")) {
            unjailMsg +=  " &afor reason: &o" + reason;
        }
        ChatUtils.sendBroadcast(unjailMsg);
        return true;
    }
}
