package com.zenya.blocksjail.commands;

import com.zenya.blocksjail.main.BlocksJail;
import com.zenya.blocksjail.storage.DataCache;
import com.zenya.blocksjail.utilities.ChatUtils;
import com.zenya.blocksjail.utilities.ConfigManager;
import com.zenya.blocksjail.utilities.DBManager;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class JailCommand implements CommandExecutor {
    private static ConfigManager configManager = ConfigManager.getInstance();
    private static DBManager dbManager = DBManager.getInstance();
    private static DataCache dataCache = DataCache.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.hasPermission("blocksjail.jail")) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        OfflinePlayer player = null;
        Integer blocks = configManager.getUnjailBlocks();
        String reason = "";

        if(args.length >= 3) {
            String[] reasonList = args;
            reasonList = (String[]) ArrayUtils.remove(reasonList, 0);
            reasonList = (String[]) ArrayUtils.remove(reasonList, 0);

            for(String str : reasonList) {
                reason = reason + " " + str;
            }
        }

        if(args.length >= 2) {
            try {
                blocks = Integer.parseInt(args[1]);

                if(blocks <= 0) {
                    throw new Exception();
                }
            } catch(Exception e) {
                ChatUtils.sendMessage(sender, "&cNumber of blocks must be a positive integer");
                return false;
            }
        }

        if(args.length >= 1) {
            for(OfflinePlayer ofp : Bukkit.getOfflinePlayers()) {
                if(args[0].equals(ofp.getName())) {
                    player = ofp;
                }
            }

            if(player == null) {
                ChatUtils.sendMessage(sender, "&cPlayer does not exist on this server");
                return true;
            }
        }

        else {
            return false;
        }

        if(player.isOnline()) {
            dataCache.registerPlayer((Player) player, blocks);
        }

        OfflinePlayer finalPlayer = player;
        Integer finalBlocks = blocks;
        new BukkitRunnable() {
            @Override
            public void run() {
                dbManager.setIsJailed(finalPlayer, true);
                dbManager.setBlocksLeft(finalPlayer, finalBlocks);
                dbManager.setBlocksTotal(finalPlayer, finalBlocks);
                dbManager.setJailedBy(finalPlayer, sender);
            }
        }.runTaskAsynchronously(BlocksJail.getInstance());

        String jailMsg = "&c" + sender.getName() + " has jailed &4" + player.getName() + " &cwith " + blocks.toString() + " blocks";
        if(!reason.equals("")) {
            jailMsg +=  " for reason: &o" + reason;
        }
        ChatUtils.sendBroadcast(jailMsg);
        return true;
    }
}
