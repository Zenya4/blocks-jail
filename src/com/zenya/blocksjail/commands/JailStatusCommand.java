package com.zenya.blocksjail.commands;

import com.zenya.blocksjail.main.BlocksJail;
import com.zenya.blocksjail.utilities.ChatUtils;
import com.zenya.blocksjail.utilities.DBManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

public class JailStatusCommand implements CommandExecutor {
    private static DBManager dbManager = DBManager.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.hasPermission("blocksjail.jailstatus")) {
            ChatUtils.sendMessage(sender, "&4You do not have permission to use this command");
            return true;
        }

        switch(args.length) {
            case 0:
                return false;
            case 1:
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        OfflinePlayer player = null;

                        for(OfflinePlayer ofp : Bukkit.getServer().getOfflinePlayers()) {
                            if(ofp.getName().equals(args[0])) {
                                player = ofp;
                            }
                        }

                        if(player == null) {
                            ChatUtils.sendMessage(sender, "&cPlayer does not exist on this server");
                            return;
                        }

                        if(!dbManager.getIsJailed(player)) {
                            ChatUtils.sendMessage(sender, "&cThis player is not jailed");
                            return;
                        }

                        Integer blocksLeft = dbManager.getBlocksLeft(player);
                        Integer blocksTotal = dbManager.getBlocksTotal(player);
                        Integer blocksMined = blocksTotal - blocksLeft;
                        String jailer = dbManager.getJailedBy(player);

                        ChatUtils.sendMessage(sender, "&4" + player.getName() + " &cis jailed by " + jailer + " &f[" + blocksMined.toString() + "/" + blocksTotal.toString() + "]");
                    }
                }.runTaskAsynchronously(BlocksJail.getInstance());
                break;

            default:
                return false;
        }
        return true;
    }
}