1.0.0:
- Initial release

1.1.0:
- Major rewrites to utilise caching for performance improvements
- Fixed `/jailstatus` not working sometimes
- Changed default jail blocks to `10000`

1.1.1:
- Made `/jailstatus` task async to prevent lag when using command
- Added `whitelisted-blocks` in config
- Updated config and `config-version`

1.1.2
- Fixed some ID duplication bugs in SQLite
- Made `/jailstatus` fully async (bug fix)
- Added logging for `/jail` and `/unjail` in console
- More miscellaneous bug fixes

1.1.3
- Fixed plugin sometimes not working for versions 1.13.x - 1.14.x

1.1.4:
- Added support for Minecraft 1.16.x
